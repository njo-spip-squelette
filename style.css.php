<?php

// #CF3F17

$couleur0 = '#FF9966';
$couleur1 = '#993300';
$couleur2 = 'black';
$colorMenu = '#FF6666';
$colorMenuHover = 'white';
$backMenuHover = '#993300';
$colorMenuActive = '#993300';
?>
*{
font-family: "Trebuchet MS", sans-serif;
}


p {
    font-size:10pt;
}
  
  
body{
	margin: 0;
	padding: 0;
	border: 0;
	overflow: hidden;
	height: 100%; 
	max-height: 100%;	
}

* html body{ /*IE6 hack*/
	padding: 180px 0 40px 0; /*Set value to (HeightOfTopFrameDiv 0 HeightOfBottomFrameDiv 0)*/
}

* html #maincontent{ /*IE6 hack*/
	height: 100%; 
	width: 100%; 
}
  

#head{
	position: absolute; 
	top: 0; 
	left: 0; 
	width: 100%; 
	height: 180px; /*Height of top frame div*/
	overflow: hidden; /*Disable scrollbars. Set to "scroll" to enable*/
	/* background-color: #882900; */
	background:<?=$couleur2?> url(images/bannernb.jpg) no-repeat center top;
	color:<?=$couleur1?>;
	text-align:center;
}

#head a img { 
    border:0;
}

#head a:hover { 
    color:<?=$couleur1?>;
    background:none;
}
 

#foot{
    position: absolute;
    width: 100%;
	top: auto;
	left: 0;
	bottom: 0; 
	height: 40px; /*Height of bottom frame div*/
	overflow: hidden; /*Disable scrollbars. Set to "scroll" to enable*/
	background-color: <?=$couleur2?>;
	color: <?=$couleur1?>;
	text-align:center;
	font-size:0.6em;	
}

#foot a {
  text-decoration:none;
  font-weight:bold;
  color:<?=$couleur1?>;
}

#foot a:hover {
  color:<?=$couleur0?>;
  background:<?=$couleur1?>;
}   


#maincontent{
	position: fixed; 
	top: 180px; /*Set top value to HeightOfTopFrameDiv*/
	left: 0;
	right: 0;
	bottom: 40px; /*Set bottom value to HeightOfBottomFrameDiv*/
	overflow: auto; 
	/* background: <?=$couleur1?>; */
	background: <?=$couleur2?> url(images/fond.jpg) no-repeat center; 
	color: <?=$couleur0?>;
}

#maincontent h1,h2,h3,h4 {
    color: <?=$couleur0?>;
}

#contenu {
	width:824px;
	margin-left:auto;
	margin-right:auto;
	text-align:justify;	
}

#contenu h1,h2,h3,h4 {
    color: <?=$couleur2?>;
}

#content
{
	clear: left;
	float: left;
	/* width: 824px; */
	padding: 20px 0;
	margin: 0 0 0 30px;
	display: inline;
}

#aside
{
	float: right;
	width: 240px;
	padding: 20px 0;
	margin: 0 20px 0 0;
	display: inline;
}



.innertube{
	margin: 15px; /*Margins for inner DIV inside each DIV (to provide padding)*/
}


#menu {
    position: absolute;
    top: auto;
	  bottom: 0; 
    margin:0;
    padding:0;
    width:100%;
    margin-left:auto;
    margin-right:auto;
    text-align:center;
    /* background:black; */
   background:<?=$couleur0?>;
   background: url(images/opaque.png) no-repeat center; 
    
}

a {
  text-decoration:none;
  font-weight:bold;
  color:<?=$couleur0?>;
}

a:hover {
  background:<?=$couleur1?>;
}   



/* menu */

ul#menu_horizontal {
   top: auto;
	 bottom: 0; 	    
   height : 30px;
   margin : 2em 0 0 0;
   padding : 0;
   color:<?=$colorMenu?>;
   list-style-type : none;
   display : inline;
}
 
ul#menu_horizontal li {    
    padding : 0 0.5em;  
    line-height : 30px;
    display : inline-block;    
    /* width: 120px; */
}
 
ul#menu_horizontal li.active {
      color:  <?=$colorMenuHover?>;
      background: black;
} 
 
ul#menu_horizontal li.bouton_gauche {
    /* float : left; */     
}
 
ul#menu_horizontal li.bouton_droite {
    /* float : right; */   
}
 
ul#menu_horizontal a {
    color:<?=$colorMenu?>;
    text-decoration : none;
    padding : 0 0.5em; 
    font :  0.8em;
}
 
ul#menu_horizontal a:hover { 
    color:<?=$colorMenuHover?>;
    background:<?=$backMenuHover?>;
}
 
ul#menu_horizontal a img { 
    border : none;
    padding :  0 0.3em;
}

.active {    
   /* border-bottom: 3px solid <?=$colorMenuActive?>; */
}

